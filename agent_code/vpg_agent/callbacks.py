
import numpy as np
import tensorflow as tf
import gym
import time
import spinup.algos.vpg.core as core
from spinup.utils.logx import EpochLogger
from spinup.utils.mpi_tf import MpiAdamOptimizer, sync_all_params
from spinup.utils.mpi_tools import mpi_fork, mpi_avg, proc_id, mpi_statistics_scalar, num_procs

#create discrete space 
from gym.spaces import Discrete, Box
from gym import spaces
from settings import s, e


eps = 0.0000000001

class VPGBuffer:
    """
    A buffer for storing trajectories experienced by a VPG agent interacting
    with the environment, and using Generalized Advantage Estimation (GAE-Lambda)
    for calculating the advantages of state-action pairs.
    """

    def __init__(self, obs_dim, act_dim, size, gamma=0.99, lam=0.95):
        self.obs_buf = np.zeros(core.combined_shape(size, obs_dim), dtype=np.float32)
        self.act_buf = np.zeros(core.combined_shape(size, ), dtype=np.float32)
        self.adv_buf = np.zeros(size, dtype=np.float32)
        self.rew_buf = np.zeros(size, dtype=np.float32)
        self.ret_buf = np.zeros(size, dtype=np.float32)
        self.val_buf = np.zeros(size, dtype=np.float32)
        self.logp_buf = np.zeros(size, dtype=np.float32)
        self.gamma, self.lam = gamma, lam
        self.ptr, self.path_start_idx, self.max_size = 0, 0, size

    def store(self, obs, act, rew, val, logp):
        """
        Append one timestep of agent-environment interaction to the buffer.
        """
        assert self.ptr < self.max_size     # buffer has to have room so you can store
        self.obs_buf[self.ptr] = obs
        self.act_buf[self.ptr] = act
        self.rew_buf[self.ptr] = rew
        self.val_buf[self.ptr] = val
        self.logp_buf[self.ptr] = logp
        self.ptr += 1

    def finish_path(self, last_val=0):
        """
        Call this at the end of a trajectory, or when one gets cut off
        by an epoch ending. This looks back in the buffer to where the
        trajectory started, and uses rewards and value estimates from
        the whole trajectory to compute advantage estimates with GAE-Lambda,
        as well as compute the rewards-to-go for each state, to use as
        the targets for the value function.

        The "last_val" argument should be 0 if the trajectory ended
        because the agent reached a terminal state (died), and otherwise
        should be V(s_T), the value function estimated for the last state.
        This allows us to bootstrap the reward-to-go calculation to account
        for timesteps beyond the arbitrary episode horizon (or epoch cutoff).
        """

        path_slice = slice(self.path_start_idx, self.ptr)
        rews = np.append(self.rew_buf[path_slice], last_val)
        vals = np.append(self.val_buf[path_slice], last_val)
        
        # the next two lines implement GAE-Lambda advantage calculation
        deltas = rews[:-1] + self.gamma * vals[1:] - vals[:-1]
        self.adv_buf[path_slice] = core.discount_cumsum(deltas, self.gamma * self.lam)
        
        # the next line computes rewards-to-go, to be targets for the value function
        self.ret_buf[path_slice] = core.discount_cumsum(rews, self.gamma)[:-1]
        
        self.path_start_idx = self.ptr

    def get(self):
        """
        Call this at the end of an epoch to get all of the data from
        the buffer, with advantages appropriately normalized (shifted to have
        mean zero and std one). Also, resets some pointers in the buffer.
        """
        assert self.ptr == self.max_size    # buffer has to be full before you can get
        self.ptr, self.path_start_idx = 0, 0
        # the next two lines implement the advantage normalization trick
        adv_mean, adv_std = mpi_statistics_scalar(self.adv_buf)
        self.adv_buf = (self.adv_buf - adv_mean) / (adv_std + eps)
        return [self.obs_buf, self.act_buf, self.adv_buf, 
                self.ret_buf, self.logp_buf]


class Model():

    def gen_input_vect(self, agent):
        input_vect = []
        coins = agent.game_state['coins']
        coins = [list(c) for c in coins]
        coins_list = np.ones(18) * (-1)

        # coins_list = []
        for i in range(len(coins)):
            coins_list[i*2] = coins[i][0]
            coins_list[i*2+1] = coins[i][1]
        x, y, _, bombs_left, _ = agent.game_state['self']
        arena = list(agent.game_state['arena'].flatten())

        input_vect += list(coins_list)
        #input_vect += arena
        input_vect += [x, y]
        return np.array(input_vect)


    def __init__(self, agent):
        
        #env_fn= None
        agent.score = 0.0
        self.epoch =0

        self.actor_critic= core.mlp_actor_critic 
        self.ac_kwargs=dict() 
        self.seed=0
        self.steps_per_epoch=1000
        self.epoch_step_num = 0
        #epochs will be ignored - set value at settings.py
        self.epochs=50


        self.gamma=0.99 
        self.pi_lr=3e-5
        self.vf_lr=1e-3 
        self.train_v_iters=80 
        self.lam=0.97 
        self.max_ep_len=200
        self.r = 0
        self.logger_kwargs=dict() 
        self.save_freq=10
        self.logger = EpochLogger(**self.logger_kwargs)

        #locals returns dict with locals - every item except 'agent' should be saved, in this case only 'self'
        self.logger.save_config(locals()['self'])
        self.seed += 10000 * proc_id()
        tf.set_random_seed(self.seed)
        np.random.seed(self.seed)

        # env = env_fn()
        # obs_dim = env.observation_space.shape
        # act_dim = env.action_space.shape
        # self.game_state = agent.game_state
        
        self.bomberman_action_space = spaces.Discrete(6)
        self.bomberman_observation_space = spaces.Box(low=0, high=255, shape=(20,), dtype=np.uint8)
        #agent.logger.info("act dim is " + str(self.bomberman_action_space.shape) + str(self.bomberman_observation_space.shape))
        self.obs_dim = self.bomberman_observation_space.shape[0]
        self.act_dim = 1

        
        # Share information about action space with policy architecture
        self.ac_kwargs['action_space'] = self.bomberman_action_space

        # Inputs to computation graph
        self.x_ph, self.a_ph = core.placeholders_from_spaces(self.bomberman_observation_space, self.bomberman_action_space)
        self.adv_ph, self.ret_ph, self.logp_old_ph = core.placeholders(None, None, None)

        # Main outputs from computation graph
        self.pi, self.logp, self.logp_pi, self.v = self.actor_critic(self.x_ph, self.a_ph, **self.ac_kwargs)

        #Need all placeholders in *this* order later (to zip with data from buffer)
        self.all_phs = [self.x_ph, self.a_ph, self.adv_ph, self.ret_ph, self.logp_old_ph]

        # Every step, get: action, value, and logprob
        self.get_action_ops = [self.pi, self.v, self.logp_pi]

        # Experience buffer
        self.local_steps_per_epoch = int(self.steps_per_epoch / num_procs())
        self.buf = VPGBuffer(self.obs_dim, self.act_dim, self.local_steps_per_epoch, self.gamma, self.lam)

        # Count variables
        self.var_counts = tuple(core.count_vars(scope) for scope in ['pi', 'v'])
        self.logger.log('\nNumber of parameters: \t pi: %d, \t v: %d\n'%self.var_counts)
        agent.logger.info('\nNumber of parameters: \t pi: %d, \t v: %d\n'%self.var_counts)


        # VPG objectives
        self.pi_loss = -tf.reduce_mean(self.logp * self.adv_ph)
        self.v_loss = tf.reduce_mean((self.ret_ph - self.v)**2)

        # Info (useful to watch during learning)
        self.approx_kl = tf.reduce_mean(self.logp_old_ph - self.logp)      # a sample estimate for KL-divergence, easy to compute
        self.approx_ent = tf.reduce_mean(-self.logp)                  # a sample estimate for entropy, also easy to compute

        # Optimizers
        self.train_pi = MpiAdamOptimizer(learning_rate=self.pi_lr).minimize(self.pi_loss)
        self.train_v = MpiAdamOptimizer(learning_rate=self.vf_lr).minimize(self.v_loss)

        self.sess = tf.Session()
        self.sess.run(tf.global_variables_initializer())

        # Sync params across processes
        self.sess.run(sync_all_params())

        # Setup model saving
        self.logger.setup_tf_saver(self.sess, inputs={'x': self.x_ph}, outputs={'pi': self.pi, 'v': self.v})

        self.start_time = time.time()
        self.r, self.d, self.ep_ret, self.ep_len, = 0, False, 0, 0

    def update(self, agent):
        agent.logger.info(" buffer get at : " + str(self.buf.max_size) + " " + str(self.buf.ptr))
        inputs = {k:v for k,v in zip(self.all_phs, self.buf.get())}
        pi_l_old, v_l_old, ent = self.sess.run([self.pi_loss, self.v_loss, self.approx_ent], feed_dict=inputs)

        # Policy gradient step
        self.sess.run(self.train_pi, feed_dict=inputs)

        # Value function learning
        for _ in range(self.train_v_iters):
            self.sess.run(self.train_v, feed_dict=inputs)

        # Log changes from update
        pi_l_new, v_l_new, kl = self.sess.run([self.pi_loss, self.v_loss, self.approx_kl], feed_dict=inputs)
        self.logger.store(LossPi=pi_l_old, LossV=v_l_old, 
                     KL=kl, Entropy=ent, 
                     DeltaLossPi=(pi_l_new - pi_l_old),
                     DeltaLossV=(v_l_new - v_l_old))
        self.epoch += 1


def end_of_episode(self):
    self.model.d = True
    """Called at the end of each game to hand out final rewards and do training.

    This is similar to reward_update, except it is only called at the end of a
    game. self.events will contain all events that occured during your agent's
    final step. You should place your actual learning code in this method.
    """

def setup(self):

    self.model = Model(self)

def act(self):
    self.model.epoch_step_num +=1
    self.logger.debug("epoch step num is now " + str(self.model.epoch_step_num))

    
    o = self.model.gen_input_vect(self)

    a, v_t, logp_t = self.model.sess.run(self.model.get_action_ops, feed_dict={self.model.x_ph: o.reshape(1,-1)})

    self.model.buf.store(o, a, self.model.r, v_t, logp_t)
    self.model.logger.store(VVals=v_t)
    
    self.next_action = s.actions[a[0]]
    # if self.next_action == 'BOMB':
    #     self.next_action = 'WAIT'
    # self.logger.info("a is now " + str(a))


    #o, r, d, _ = env.step(a[0])
    self.model.r = custom_calculate_reward(agent = self)
    self.model.ep_ret += self.model.r
    self.model.ep_len += 1
    
    self.model.t = self.model.epoch_step_num
    
    if (self.model.epoch_step_num == self.model.steps_per_epoch ):
        end_of_epoch(self)

    
    terminal = self.model.d or (self.model.ep_len == self.model.max_ep_len)
    if terminal or (self.model.t==self.model.local_steps_per_epoch-1):
        if not(terminal):
            print('Warning: trajectory cut off by epoch at %d steps.'%self.model.ep_len)
        # if trajectory didn't reach terminal state, bootstrap value target
        last_val = self.model.r if self.model.d else self.model.sess.run(self.model.v, feed_dict={self.model.x_ph: o.reshape(1,-1)})
        self.model.buf.finish_path(last_val)
        if terminal:
            # only save EpRet / EpLen if trajectory finished
            self.model.logger.store(EpRet=self.model.ep_ret, EpLen=self.model.ep_len)
        o, self.model.r, self.model.d, self.model.ep_ret, self.model.ep_len = self.model.gen_input_vect(self), 0, False, 0, 0


    #model.d can be set to True by end_of_episode()
    if self.model.d == True:
        self.model.d = False


def reward_update(self):
    # Save model
    self.model.r = custom_calculate_reward(agent = self)
    self.model.ep_ret += self.model.r

    self.logger.info("reward is " +  str(self.model.ep_ret))
    
def end_of_epoch(self):
    #this block will be executed as often as the outer for loop (loop over epochs) from vpg orig
    self.model.epoch_step_num = 0
    if (self.model.epoch % self.model.save_freq == 0) or (self.model.epoch == self.model.epochs-1):
        o = self.model.gen_input_vect(self)

        self.model.logger.save_state({'env': o}, None)

    self.model.update(self)
    
    #Log info about epoch
    self.model.logger.log_tabular('Epoch', self.model.epoch)
    self.model.logger.log_tabular('EpRet', with_min_and_max=True)
    self.model.logger.log_tabular('EpLen', average_only=True)
    self.model.logger.log_tabular('VVals', with_min_and_max=True)
    self.model.logger.log_tabular('TotalEnvInteracts', (self.model.epoch+1)*self.model.steps_per_epoch)
    self.model.logger.log_tabular('LossPi', average_only=True)
    self.model.logger.log_tabular('LossV', average_only=True)
    self.model.logger.log_tabular('DeltaLossPi', average_only=True)
    self.model.logger.log_tabular('DeltaLossV', average_only=True)
    self.model.logger.log_tabular('Entropy', average_only=True)
    self.model.logger.log_tabular('KL', average_only=True)
    self.model.logger.log_tabular('Time', time.time()-self.model.start_time)
    self.model.logger.dump_tabular()

def custom_calculate_reward(agent):
    #coins = agent.game_state['coins']
    #x, y, _, bombs_left = agent.game_state['self']
    #old_dist = self.coindist
    events = agent.events
    #self.coindist = np.min(np.sum(np.abs(np.asarray([x, y]) - np.asarray(coins)), axis = 1))
    reward = 0
    if e.COIN_COLLECTED in events:
        reward += 2
    if e.KILLED_SELF in events:
        reward -= 5
    if e.WAITED in events:
        reward += 2
    if e.INVALID_ACTION in events:
        reward -= 0
    if e.KILLED_OPPONENT in events:
        reward += 5
    agent.score += reward
    return reward