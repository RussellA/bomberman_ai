\babel@toc {english}{}
\contentsline {chapter}{\nonumberline List of Figures}{1}{chapter*.4}
\contentsline {chapter}{\nonumberline List of Tables}{2}{chapter*.5}
\contentsline {chapter}{\numberline {1}Introduction}{3}{chapter.6}
\contentsline {section}{\numberline {1.1}Bomberman}{3}{section.7}
\contentsline {section}{\numberline {1.2}Machine learning}{3}{section.8}
\contentsline {section}{\numberline {1.3}Reinforcement Learning}{4}{section.9}
\contentsline {chapter}{\numberline {2}Methods}{6}{chapter.10}
\contentsline {section}{\numberline {2.1}Tree-based Models}{6}{section.11}
\contentsline {subsection}{\numberline {2.1.1}Gradient Boosting}{6}{subsection.12}
\contentsline {subsection}{\numberline {2.1.2}Comparison of Random Forest and Gradient Boosting}{8}{subsection.20}
\contentsline {section}{\numberline {2.2}Neural Networks}{9}{section.21}
\contentsline {subsection}{\numberline {2.2.1}NN Agent using Keras}{10}{subsection.26}
\contentsline {subsubsection}{\nonumberline Structure of the NN}{11}{section*.29}
\contentsline {subsubsection}{\nonumberline Activation Function}{12}{section*.31}
\contentsline {subsubsection}{\nonumberline Initializer}{12}{section*.33}
\contentsline {subsubsection}{\nonumberline Optimizer}{13}{section*.34}
\contentsline {subsubsection}{\nonumberline Training}{14}{section*.39}
\contentsline {subsubsection}{\nonumberline Results and further improvement}{14}{section*.40}
\contentsline {section}{\numberline {2.3}Proximal Policy Optimization}{15}{section.41}
\contentsline {subsection}{\numberline {2.3.1}Introduction to Proximal Policy Optimization}{15}{subsection.42}
\contentsline {subsection}{\numberline {2.3.2}Application of PPO on Bomberman}{16}{subsection.44}
\contentsline {chapter}{\numberline {3}Gradient Boosting Agent}{17}{chapter.46}
\contentsline {section}{\numberline {3.1}Coin Collection}{17}{section.49}
\contentsline {section}{\numberline {3.2}Crate Navigation}{18}{section.55}
\contentsline {section}{\numberline {3.3}Playing with opponents}{21}{section.69}
\contentsline {chapter}{\numberline {4}Training}{23}{chapter.70}
\contentsline {section}{\numberline {4.1}Rewards}{23}{section.71}
\contentsline {section}{\numberline {4.2}Hyperparameter Fitting}{23}{section.72}
\contentsline {subsubsection}{\nonumberline Loss}{26}{section*.82}
\contentsline {subsubsection}{\nonumberline Number of steps}{26}{section*.84}
\contentsline {subsubsection}{\nonumberline Maximum duplicates}{26}{section*.86}
\contentsline {subsubsection}{\nonumberline Shrinking Factor}{27}{section*.88}
\contentsline {subsubsection}{\nonumberline Decay}{27}{section*.90}
\contentsline {subsubsection}{\nonumberline Maximum tree depth}{28}{section*.92}
\contentsline {subsubsection}{\nonumberline Minimum number of samples per leaf}{28}{section*.94}
\contentsline {subsubsection}{\nonumberline Minimum number of samples to split a node}{28}{section*.96}
\contentsline {subsubsection}{\nonumberline Number of Estimators}{29}{section*.98}
\contentsline {subsubsection}{\nonumberline Learning Rate}{29}{section*.100}
\contentsline {subsubsection}{\nonumberline Deep First Tree}{29}{section*.102}
\contentsline {section}{\numberline {4.3}Training the final agent with the selected hyperparameters}{30}{section.104}
\contentsline {chapter}{\numberline {5}Outlook}{32}{chapter.108}
\contentsline {section}{\numberline {5.1}Improving the Agent}{32}{section.109}
\contentsline {section}{\numberline {5.2}Improving the Final Project Challenge}{32}{section.110}
